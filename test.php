<?php
/*
Plugin Name: TEST
Description: TEST
Version:     1.1
Author:      Pham Cong Son
Author URI:  http://phamcongson.com
*/	

require __DIR__.'/plugin-update-checker-4.4/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/phamcongsonit/test',
	__FILE__,
	'test-pcs'
);

$myUpdateChecker->setAuthentication('hEaV-y9BVSUYgd9rNcQX');
//Optional: Set the branch that contains the stable release.
$myUpdateChecker->setBranch('master');